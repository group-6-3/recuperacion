/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Taller;

/**
 *
 * @author vitic
 */
public class Vehiculo {
    // Atributos
    protected String serieVehiculo;
    protected String tipoMotor;
    protected String marca;
    protected String modelo;
    
    // Constructores
    public Vehiculo() {
        this.serieVehiculo = "";
        this.tipoMotor = "";
        this.marca = "";
        this.modelo = "";
    }
    
    public Vehiculo(String serieVehiculo, String tipoMotor, String marca, String modelo) {
        this.serieVehiculo = serieVehiculo;
        this.tipoMotor = tipoMotor;
        this.marca = marca;
        this.modelo = modelo;
    }
    
    // Encapsulado
    public String getSerieVehiculo() {
        return serieVehiculo;
    }

    public void setSerieVehiculo(String serieVehiculo) {
        this.serieVehiculo = serieVehiculo;
    }

    public String getTipoMotor() {
        return tipoMotor;
    }

    public void setTipoMotor(String tipoMotor) {
        this.tipoMotor = tipoMotor;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    
    // Métodos
    
    
}
