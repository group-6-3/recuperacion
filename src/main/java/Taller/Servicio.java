/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Taller;

/**
 *
 * @author vitic
 */
public class Servicio {
    // Atributos
    private int numeroServicio;
    private String fechaServicio;
    private String tipoServicio;
    private Vehiculo vehiculo;
    private String descAuto;
    private float costoBaseMant;
    
    // Constructores
    public Servicio() {
        this.numeroServicio = 0;
        this.fechaServicio = "";
        this.tipoServicio = "";
        this.vehiculo = null;
        this.descAuto = "";
        this.costoBaseMant = 0.0f;
    }
    
    public Servicio(int numeroServicio, String fechaServicio, String tipoServicio, Vehiculo vehiculo, String descAuto, float costoBaseMant) {
        this.numeroServicio = numeroServicio;
        this.fechaServicio = fechaServicio;
        this.tipoServicio = tipoServicio;
        this.vehiculo = vehiculo;
        this.descAuto = descAuto;
        this.costoBaseMant = costoBaseMant;
    }
    
    // Encapsulado
    public int getNumeroServicio() {
        return numeroServicio;
    }

    public void setNumeroServicio(int numeroServicio) {
        this.numeroServicio = numeroServicio;
    }

    public String getFechaServicio() {
        return fechaServicio;
    }

    public void setFechaServicio(String fechaServicio) {
        this.fechaServicio = fechaServicio;
    }

    public String getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public String getDescAuto() {
        return descAuto;
    }

    public void setDescAuto(String descAuto) {
        this.descAuto = descAuto;
    }

    public float getCostoBaseMant() {
        return costoBaseMant;
    }

    public void setCostoBaseMant(float costoBaseMant) {
        this.costoBaseMant = costoBaseMant;
    }
    
    // Métodos
    public float calcularCostoMantenimiento(){
        float incremento = 1.0f;
        
        switch(this.vehiculo.getTipoMotor()) {
            case "1 - V4": incremento = 1.25f; break;
            case "2 - V6": incremento = 1.50f; break;
            case "3 - V8": incremento = 2.50f; break;
            case "4 - V10": incremento = 3.0f; break;
        }
        
        return this.costoBaseMant * incremento;
    }
    
    public float calcularImpuesto(){
        return this.calcularCostoMantenimiento() * 0.16f;
    }
}
