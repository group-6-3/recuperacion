/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Taller;

/**
 *
 * @author vitic
 */
public class VehiculoCarga extends Vehiculo{
    // Atributos
    private float numTonCarga;
    
    // Constructores
    public VehiculoCarga() {
        super();
        this.numTonCarga = 0.0f;
    }
    
    public VehiculoCarga(String serieVehiculo, String tipoMotor, String marca, String modelo, float numTonCarga) {
        super(serieVehiculo, tipoMotor, marca, modelo);
        this.numTonCarga = numTonCarga;
    }
    
    // Encapsulado
    public float getNumTonCarga() {
        return numTonCarga;
    }

    public void setNumTonCarga(float numTonCarga) {
        this.numTonCarga = numTonCarga;
    }
    
    // Métodos
    
}
